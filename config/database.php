<?php
class Database {
    private $host_name = "localhost";
    private $db_name   = "ars";
    private $username  = "root";
    private $password  = "";
    public $connect;

    public function getConnection() {
        $this->connect = null;
        
        try {
            $this->connect = new PDO("mysql:host=" . $this->host_name . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->connect->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->connect;

    }
}
?>