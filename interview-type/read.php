<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../objects/interview-type.php';

$database = new Database();

$db = $database->getConnection();

$interviewType = new InterviewType($db);

$stmt = $interviewType->read();
$num = $stmt->rowCount();

if($num>0){
    $interview_type_array = array();
    $interview_type_array["types"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $interview_type_item=array(
            "id" => $id,
            "name" => $name
        );
 
        array_push($interview_type_array["types"], $interview_type_item);
    }
    http_response_code(200);
    echo json_encode($interview_type_array);
} 
else {
    http_response_code(404);

    echo json_encode(
    array("message" => "No interviews found.")
    );  
}