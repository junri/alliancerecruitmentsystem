<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../objects/interview-schedule.php';

$database = new Database();

$db = $database->getConnection();

$interview = new InterviewSchedule($db);

$stmt = $interview->read();
$num = $stmt->rowCount();

if($num>0){
    $interview_array = array();
    $interview_array["records"] = array();
    $applicant_array = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $interview_item=array(
            "id" => $id,
            "applicantID" => $applicantID,
            "firstname" => $firstname,
            "middlename" => $middlename,
            "lastname" => $lastname,
            "interviewID" => $interviewID,
            "interviewTypeName" => $interviewTypeName,
            "location" => $location,
            "schedule" => $schedule,
            "status" => $status
        );
 
        array_push($interview_array["records"], $interview_item);
    }
    for($i=0; $i<count($interview_array["records"]); $i++){
        if($interview_array["records"][$i]["interviewTypeName"] == 'Technical Interview'){
            array_push($applicant_array, $interview_array["records"][$i]);
        } else {
            for($j=0; $j<count($applicant_array); $j++){
                if($applicant_array[$j]["id"] == $interview_array["records"][$i]["applicantID"]){
                    $applicant_array[$j]["finalInterview"] = $interview_array["records"][$i]["interviewID"];
                    $applicant_array[$j]["finalDate"] = $interview_array["records"][$i]["schedule"];
                    $applicant_array[$j]["finaloc"] = $interview_array["records"][$i]["location"];
                    $applicant_array[$j]["finalStatus"] = $interview_array["records"][$i]["status"];
                }
            }
        }
    }
    http_response_code(200);
    echo json_encode($applicant_array);
} 
else {
    http_response_code(404);

    echo json_encode(
    array("message" => "No interviews found.")
    );  
}