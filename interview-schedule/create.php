<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Content-Type: text/html; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/interview-schedule.php';
 
$database = new Database();
$db = $database->getConnection();
 
$interview = new InterviewSchedule($db);
 
// get posted data
if(isset($_POST["submit"])){
    $data = new \stdClass();
    $data->applicantID = $_POST["applicantID"];
    $data->interviewID = $_POST["interviewID"];
    $data->interviewType = $_POST["interviewType"];
    $data->location = $_POST["location"];
    $data->schedule = $_POST["schedule"];
    $data->status = $_POST["status"];
} else {
    $data = json_decode(file_get_contents("php://input"));
}


// make sure data is not empty
// if(!empty($data->applicantID)){
//     $appID = "true";
// }
if(
   !empty($data->applicantID)&&
   !empty($data->interviewID)&&
   !empty($data->interviewType)&&
   !empty($data->location)&&
   !empty($data->schedule)&&
   !empty($data->status)
){
 
    // set product property values
    $interview->applicantID = $data->applicantID;
    $interview->interviewID = $data->interviewID;
    $interview->interviewType = $data->interviewType;
    $interview->location = $data->location;
    $interview->schedule = $data->schedule;
    $interview->status = $data->status;
 
    // create the product
    if($interview->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "Interiew was set."));
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to set interview."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to set interview. Data is incomplete.", "data" => $data));
}
?>