<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/interview-schedule.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$interview = new InterviewSchedule($db);
 
// set ID property of record to read
$interview->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of product to be edited
$stmt = $interview->readOne();
$num = $stmt->rowCount();
 
if($num > 0){
    $interview_array = array();
    $interview_array["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
 
        $interview_item=array(
            "id" => $id,
            "applicantID" => $applicantID,
            "interviewID" => $interviewID,
            "interviewTypeName" => $interviewTypeName,
            "location" => $location,
            "schedule" => $schedule,
            "status" => $status
        );

        array_push($interview_array["records"], $interview_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($interview_array);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user product does not exist
    echo json_encode(array("message" => "Product does not exist."));
}
?>