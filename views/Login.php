<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charset="UTF-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/LoginStyleSheet.css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
    </head>
    <body>
        <div class="container-fluid">
            <div class="login-form">
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <h5> Alliance Recruitment System </h5>
                    </div>
                </div>
                <form action="../login/Read.php" method="post">
                    <h2 class="text-center">Log in</h2>       
                    <div class="form-group">
                        <input type="text" name="id" class="form-control" placeholder="ID" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" name="pin" class="form-control" placeholder="PIN" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,4);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,4);" maxlength="4" required="required">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-dark btn-block">Log in</button>
                    </div>
                    <div class="clearfix">
                        <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
                        <a href="#" class="pull-right">Forgot Password?</a>
                    </div>        
                </form>
            </div>
        </div>
        <script language="javascript" type="text/javascript">
            function limitText(limitField, limitCount, limitNum) {
                if (limitField.value.length > limitNum) {
                    limitField.value = limitField.value.substring(0, limitNum);
                } else {
                    limitCount.value = limitNum - limitField.value.length;
                }
            }
        </script>
    </body>
</html>