<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/AddNewApplicantStyleSheet.css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <a href="viewallapplicants.php" class="navbar-brand small ml-5">Alliance Recruitment System</a>
            <ul class="navbar-nav mr-5">
                <ul class="nav justify-content-end ">
                    <li class="nav-item mr-3">
                    <a class="nav-link" href="viewallapplicants.php">Applicants</a>
                    </li>
                    <li class="nav-item mr-3">
                    <a class="nav-link" href="interview.php">Interviews</a>
                    </li>
                </ul>
            </ul>
        </nav>
        <form action="/alliancerecruitmentsystem-api/applicant/CreateApplicant.php" method="GET">
            <div class="container-fluid">
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-5">
                            <h1> Add Applicant </h1>
                        </div>
                    </div>
                </div>
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-5">
                            <h4> Basic Info </h4>
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-4">
                            Last Name 
                            <input type="text" name="lastname" class="col-md-7 ml-5" required="required">
                        </div>
                        <div class="col-md-4">
                            First Name 
                            <input type="text" name="firstname" class="col-md-7 ml-1" required="required">
                        </div>
                        <div class="col-md-4">
                            Middle Name 
                            <input type="text" name="middlename" class="col-md-7 ml-1" required="required"> 
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-4">
                            Application Date
                            <input type="date" name="application_date" class="col-md-7 ml-1" required="required">
                        </div>
                        <div class="col-md-4">
                            Birthdate
                            <input type="date" name="birthdate" class="col-md-7 ml-3" required="required">
                        </div>
                        <div class="col-md-4">
                            Status &nbsp
                            <input type="text" name="status" class="col-md-7 ml-5 text-center" required="required" value="New" disabled="disabled">
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-4">
                            Position Applied
                            <input type="text" name="position_applied" class="col-md-7 ml-1" required="required">
                        </div>
                        <div class="col-md-4">
                            Gender
                            <div class="form-check-inline ml-4">
                                <label class="form-check-label">
                                    <input type="radio" name="gender" class="form-check-input" value="Male" required="required">Male
                                    <input type="radio" name="gender" class="form-check-input ml-1" value="Female" required="required">Female
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-5">
                            <h4> Academic Records </h4>
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-1">
                            School
                        </div>
                        <div class="col-md-5 ml-3">
                            <input type="text" name="school" class="col-md-12 ml-2" required="required">
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-1">
                            Course
                        </div>
                        <div class="col-md-5 ml-3">
                            <input type="text" name="course" class="col-md-12 ml-2" required="required">
                        </div>
                    </div>
                </div>
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-5">
                            <h4> Exam Records </h4>
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" class="col-md-12 mt-1" disabled="disabled">
                        </div>
                        <div class="col-md-2">
                            <button class="button form-control"><i class="fa fa-upload"></i> Upload File </button>
                        </div>
                    </div>
                </div>
                <div class="container mt-2">
                    <div class="row">    
                        <div class="col-md-3">
                        <button type="submit" class="button form-control" name="submit"> Save All Changes </button>     
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>