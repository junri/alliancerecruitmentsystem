<?php
    $conn = new mysqli("localhost", "root", "", "ars");
    if(isset($_GET['submit'])){
        $applicantId = $_GET['applicantId'];
    }else{
        $applicantId = "";
    }

    if(!empty($_POST['interviewID']) &&
        !empty($_POST['applicantID']) &&
        !empty($_POST['schedule']) &&
        !empty($_POST['status']) &&
        !empty($_POST['interviewType']) &&
        !empty($_POST['location'])){
            
           print_r($_POST);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/ViewApplicantDetailStylesheet.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-dark bg-dark">
        <a href="viewallapplicants.php" class="navbar-brand small ml-5">Alliance Recruitment System</a>
        <ul class="navbar-nav mr-5">
            <ul class="nav justify-content-end ">
                <li class="nav-item mr-3">
                <a class="nav-link" href="viewallapplicants.php">Applicants</a>
                </li>
                <li class="nav-item mr-3">
                <a class="nav-link" href="interview.php">Interviews</a>
                </li>
            </ul>
        </ul>
    </nav>
    <?php
            $sql = "SELECT * FROM applicants WHERE id='$applicantId'";
            $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
        ?>
        <div class="container-fluid">
            <div class="container mt-3"> 
                    <button onclick="location.href='/alliancerecruitmentsystem-api/views/viewallapplicants.php'" class="btn btn-dark btn-sm"><span class="fa fa-chevron-circle-left"></span> List of applicants</button>
                <h1 class="text-left mt-3"><?php echo $row['lastname'] .', '.$row['firstname'].' '.strtoupper(substr($row['middlename'],0,1)).'.';?></h1>
            </div>
            <div class="container mt-3"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Name
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['lastname'] .', '.$row['firstname'].' '.strtoupper(substr($row['middlename'],0,1)).'.';?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Course
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['course'];?>
                            </div>
                        </div>       
                    </div>
                <div>  
            </div>

            <div class="container mt-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Application Date
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['application_date'];?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                School
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['school'];?>
                            </div>
                        </div>       
                    </div>
                <div>  
            </div>

            <div class="container mt-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Position Applied
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['position_applied'];?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                    Birthdate
                            </div>
                                <div class="col-md-8 strong">
                                    <?php echo $row['birthdate'];?>
                                </div>
                        </div>       
                    </div>
                <div>  
            </div>

            <div class="container mt-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Status
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['status'];?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Gender
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['gender'];?>
                            </div>
                        </div>       
                    </div>
                <div>  
            </div>
        </div>
        <div class="container mt-5">     
            <div class="row">   
                <h2 class="text-left"> Interview Schedules </h2> 
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <?php
                                $th_labels = array("Interviewer", "Type", "Date", "Time", "Status");

                                foreach($th_labels as $label){
                                    echo '<th>' .$label. '</th>';
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                                $response = @file_get_contents('http://localhost/alliancerecruitmentsystem-api/interview-schedule/read-one.php?id='. $applicantId);
                                if (strpos($http_response_header[0], "200")) { 
                                    $interviews = json_decode($response, true);
                                    foreach($interviews["records"] as $interview){
                                        echo '<tr>';
                                            echo '<td>'. $interview["interviewID"] .'</td>';
                                            echo '<td>'. $interview["interviewTypeName"] .'</td>';
                                            $sched = explode(" ",$interview["schedule"]);
                                            echo '<td>'. $sched[0] .'</td>';
                                            echo '<td>'. $sched[1] .'</td>';
                                            
                                            echo '<td>'. $interview["status"].'</td>';
                                        echo '</tr>';
                                    }
                                    if(count($interviews["records"]) < 2){
                                        if(count($interviews["records"]) == 0 || $interviews["records"][0]["status"] == "Passed"){
                                            echo '<tr>';
                                                echo '<td class="text-center" colspan="5" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus"></i> Add Interview</td>';  
                                            echo '</tr>';
                                        }
                                    }
                                 } else { 
                                    echo '<tr>';
                                        echo '<td class="text-center" colspan="5" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus text-success"></i> Add Interview</td>';    
                                    echo '</tr>';
                                 }
                                
                               
                        ?>
                    </tbody>
                </table>        
            </div>
        </div>
        <div class="container">
            <div class="d-flex justify-content-center mb-3">
                <div>
                    <button type="button" class="btn btn-sm btn-danger" onclick="deleteApplicantDetails  ()">    
                            Delete 
                        </button>
                </div>  
                <div class="ml-2"> 
                    <button type="button" class="btn btn-sm btn-primary" onclick="window.location.href = '/alliancerecruitmentsystem-api/views/UpdateApplicantDetails.php?applicantId=<?php echo $applicantId;?>';">Edit Applicant</button>
                </div>   
            </div>
        </div>
        <script>
            function deleteApplicantDetails() {
                var ask = confirm("Are you sure you want to delete '<?php echo $applicantId ?>'");
                if(ask==true){
                    window.location.href = 'crud/DeleteApplicant.php?applicantId=<?php echo $applicantId;?>';
                }else{
                }
                
            }
        </script>
        <?php
            }
            } else {

            }
                mysqli_close($conn);
        ?> 

        <!-- Modal -->
        <!-- http://localhost/alliancerecruitmentsystem-api/interview-schedule/create.php -->
        <form enctype="application/x-www-form-urlencoded" action="http://localhost/alliancerecruitmentsystem-api/interview-schedule/create.php" name="data" method="post" >
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Interview Schedule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div class="container">
                            <div class="form-group">
                                <input type="text" value="<?php echo $applicantId; ?>" name="applicantID" class="form-control">
                                <label for="sel1">Type of Interview:</label>
                                <select name="interviewType" class="form-control">
                                    <?php
                                        $response = @file_get_contents('http://localhost/alliancerecruitmentsystem-api/interview-type/read.php');
                                        $types = json_decode($response, true);
                                        foreach($types["types"] as $type){
                                            echo '<option value="'.$type["id"].'">'.$type["name"].'</option>';
                                        }
                                    ?>
                                </select>
                                <label for="interviewer" class="mt-3">Interviewer:</label>
                                <input type="text" name="interviewID" class="form-control">
                                <label for="schedule" class="mt-3">Schedule:</label>
                                <input type="datetime-local" name="schedule" class="form-control">
                                <label for="location" class="mt-3">Location:</label>
                                <input type="text" name="location" class="form-control">
                                <input type="text" name="status" value="Pending" class="form-control" hidden>
                            </div>
                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit" name="submit">Save changes</button>
                </div>
                </div>
            </div>
            </div>
        </form>
    </body>
</html>