<?php
    session_start();
    $conn = new mysqli("localhost", "root", "", "ars");
?>
<!DOCTYPE html>
<html>
    <head>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/ViewAllApplicantStyleSheet.css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <a href="viewallapplicants.php" class="navbar-brand small ml-5">Alliance Recruitment System</a>
            <ul class="navbar-nav mr-5">
                <ul class="nav justify-content-end ">
                    <li class="nav-item mr-3">
                    <a class="nav-link" href="viewallapplicants.php">Applicants</a>
                    </li>
                    <li class="nav-item mr-3">
                    <a class="nav-link" href="interview.php">Interviews</a>
                    </li>
                </ul>
            </ul>
        </nav>
        <div class="container mt-5">
            <!-- <div class="container container-down"> -->
                <h1 class="d-flex justify-content-left mb-4"> All Applicants </h1>
            <!-- </div> -->
                <div class="form-group">
                    <div class="row">
                            <div class="col-md-12">
                                <div class="top">
                                    <div class="td_1">
                                        <div class="form-group">
                                            <span class="fa fa-search form-control-icon"></span>
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div> 
                                    </div>
                                    <div class="td_2">
                                        <div class="dropdown">
                                            <small>
                                                View by
                                            </small>
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                Day
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Week</a>
                                                <a class="dropdown-item" href="#">Month</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="td_3">
                                        <button type="button" class="form-control" onclick="window.location.href='AddNewApplicant.php'">Add New Applicant</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <table class="table table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Application Date</th>
                                    <th>Position Applied</th>
                                    <th>Status</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT * FROM applicants";
                                    $result = mysqli_query($conn, $sql);
                                        
                                    if (mysqli_num_rows($result) > 0) {
                                                // output data of each row
                                    while($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <form action="ViewApplicantDetails.php" method="GET">
                                    <tr>
                                        <input type="hidden" name="applicantId" value="<?php echo $row["id"];?>">
                                        <td><?php echo $row["id"]; ?></td> 
                                        <td><?php echo $row["firstname"].' '.$row["middlename"].' '.$row["lastname"]; ?></td>
                                        <td><?php echo $row["application_date"]; ?></td>
                                        <td><?php echo $row["position_applied"]; ?></td>
                                        <td><?php echo $row["status"]; ?></td> 
                                        <td><button class="btn" name="submit">View Details <i class="fa fa-chevron-circle-right"></i> </button></td>                    
                                    </tr>
                                </form>
                                <?php
                                    }
                                    } else {
                                    }
                                    mysqli_close($conn);
                                ?>  
                            </tbody>
                    </table>           
                </div>               
        </div>
    </body>
</html>
