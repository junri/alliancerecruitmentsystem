<?php
    session_start();
    $conn = new mysqli("localhost", "root", "", "ars");
?>
<html>
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/ViewAllApplicantStyleSheet.css">
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <a href="viewallapplicants.php" class="navbar-brand small ml-5">Alliance Recruitment System</a>
        <ul class="navbar-nav mr-5">
            <ul class="nav justify-content-end ">
                <li class="nav-item mr-3">
                <a class="nav-link" id="applicant" href="viewallapplicants.php">Applicants</a>
                </li>
                <li class="nav-item mr-3">
                <a class="nav-link" id="interview" href="interview.php">Interviews</a>
                </li>
            </ul>
        </ul>
    </nav>
        
    <div class="container mt-5">
        <h1 class="d-flex justify-content-left mb-4">Interview Schedules</h1>
        <div class="form-group">
            <div class="row">
                    <div class="col-md-12">
                        <div class="top">
                            <div class="td_1">
                                <div class="form-group">
                                    <span class="fa fa-search form-control-icon"></span>
                                    <input type="text" class="form-control" placeholder="Search">
                                </div> 
                            </div>
                            <div class="td_2">
                                <div class="dropdown">
                                    <small>
                                        View by
                                    </small>
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                        Day
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Week</a>
                                        <a class="dropdown-item" href="#">Month</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <?php
                        $th_array = array("Applicant Name", "Technical Interviewer", "Date of Technical Interview", "Final Interviewer", "Date of Final Interview", "");
                        foreach($th_array as $th){
                            echo '<th>'.$th.'</th>';
                        }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                    $response = @file_get_contents('http://localhost/alliancerecruitmentsystem-api/interview-schedule/read.php');
                    if (strpos($http_response_header[0], "200")) { 
                        $applicantsArray = json_decode($response, true);
                            
                        foreach($applicantsArray as $applicantArray){
                            echo '<tr>';
                                echo '<td>'.$applicantArray["lastname"]. ', '.$applicantArray["firstname"]. ' '. $applicantArray["middlename"][0].'.</td>';
                                echo '<td>'.$applicantArray["interviewID"].'</td>';
                                echo '<td>'.$applicantArray["schedule"].'</td>';
                                if( isset($applicantArray["finalInterview"])){
                                    echo '<td>'.$applicantArray["finalInterview"].'</td>';
                                } else {
                                    echo '<td> N/A </td>';
                                }
                                if( isset($applicantArray["finalInterview"])){
                                    echo '<td>'.$applicantArray["finalDate"].'</td>';
                                } else {
                                    echo '<td> N/A </td>';
                                }
                                echo '<td class="small"><a href="/alliancerecruitmentsystem-api/views/interview-details.php?applicantID='.$applicantArray["applicantID"].'">View Details </a></td>';
                            echo'</tr>';
                        }
                    } else { 
                        echo '<tr>';
                            echo '<td class="text-center" colspan="5">No Applicants for Interview</td>';    
                        echo '</tr>';
                    }  
                ?>
            </tbody>
        </table>
    </div>

</body>
</html>