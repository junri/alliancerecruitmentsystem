

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <a href="viewallapplicants.php" class="navbar-brand small ml-5">Alliance Recruitment System</a>
        <ul class="navbar-nav mr-5">
            <ul class="nav justify-content-end ">
                <li class="nav-item mr-3">
                <a class="nav-link" href="viewallapplicants.php">Applicants</a>
                </li>
                <li class="nav-item mr-3">
                <a class="nav-link" href="interview.php">Interviews</a>
                </li>
            </ul>
        </ul>
    </nav>
    <?php
            $conn = new mysqli("localhost", "root", "", "ars");
            $applicantId = $_GET["applicantID"];
            $sql = "SELECT * FROM applicants WHERE id='$applicantId'";
            $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
        ?>
        <div class="container-fluid">
            <div class="container mt-3"> 
                    <button onclick="location.href='/alliancerecruitmentsystem-api/views/interview.php'" class="btn btn-dark btn-sm"><span class="fa fa-chevron-circle-left"></span> List of interview schedules</button>
                <h1 class="text-left mt-3"><?php echo $row['lastname'] .', '.$row['firstname'].' '.strtoupper(substr($row['middlename'],0,1)).'.';?></h1>
            </div>
            <div class="container mt-3"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Name
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['lastname'] .', '.$row['firstname'].' '.strtoupper(substr($row['middlename'],0,1)).'.';?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Course
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['course'];?>
                            </div>
                        </div>       
                    </div>
                <div>  
            </div>

            <div class="container mt-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Application Date
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['application_date'];?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                School
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['school'];?>
                            </div>
                        </div>       
                    </div>
                <div>  
            </div>

            <div class="container mt-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Position Applied
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['position_applied'];?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                    Birthdate
                            </div>
                                <div class="col-md-8 strong">
                                    <?php echo $row['birthdate'];?>
                                </div>
                        </div>       
                    </div>
                <div>  
            </div>

            <div class="container mt-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Status
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['status'];?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Gender
                            </div>
                            <div class="col-md-8 strong">
                                <?php echo $row['gender'];?>
                            </div>
                        </div>       
                    </div>
                <div>  
            </div>
        </div>
        <div class="container mt-5">     
            <div class="row">   
                <h2 class="text-left"> Interview Schedules </h2> 
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <?php
                                $th_labels = array("Interviewer", "Type", "Date", "Time", "Location", "Status");

                                foreach($th_labels as $label){
                                    echo '<th>' .$label. '</th>';
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                            $response = file_get_contents('http://localhost/alliancerecruitmentsystem-api/interview-schedule/read-one.php?id='. $applicantId);

                            $interviews = json_decode($response, true);
                            foreach($interviews["records"] as $interview){
                                echo '<tr>';
                                    echo '<td>'. $interview["interviewID"] .'</td>';
                                    $sched = explode(" ",$interview["schedule"]);
                                    echo '<td>'. $interview["interviewTypeName"] .'</td>';
                                    echo '<td>'. $sched[0] .'</td>';
                                    echo '<td>'. $sched[1] .'</td>';
                                    echo '<td>'. $interview["location"] .'</td>';
                                    echo '<td>'. $interview["status"].'</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>        
            </div>
        </div>
        <?php
            }
            } else {

            }
                mysqli_close($conn);
        ?> 

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</body>
</html>