<?php
class InterviewType {
    private $id;
    private $name;

    public function __construct($db){
        $this->connect = $db;
    }

    function read(){
        $query = "SELECT
                    *
                FROM 
                    interviewtype";

                $stmt = $this->connect->prepare($query);

                $stmt->execute();

                return $stmt;
    }
    
}
