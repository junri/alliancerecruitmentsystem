<?php
class InterviewSchedule {
    private $connect;
    private $table_name = "interview";

    public $id;
    public $applicantID;
    public $firstname;
    public $middlename;
    public $lastname;
    public $interviewID;
    public $interviewType;
    public $interviewTypeName;
    public $location;
    public $schedule;
    public $status;

    public function __construct($db){
        $this->connect = $db;
    }
    
    function read() {
        $query = "SELECT
                    a.firstname as firstname, a.middlename as middlename, a.lastname as lastname, it.name as interviewTypeName, i.id, i.applicantID, i.interviewID, i.location, i.schedule, i.status
                FROM 
                   " . $this->table_name . " i
                    LEFT JOIN
                        interviewtype it
                            ON i.interviewType = it.id
                    LEFT JOIN
                        applicants a
                            ON i.applicantID = a.id
                ORDER BY
                    i.id ASC";

        $stmt = $this->connect->prepare($query);

        $stmt->execute();

        return $stmt;
    }
   
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    applicantID=:applicantID, interviewID=:interviewID, interviewType=:interviewType, location=:location, schedule=:schedule, status =:status";
    
        // prepare query
        $stmt = $this->connect->prepare($query);
    
        // sanitize
        $this->applicantID=htmlspecialchars(strip_tags($this->applicantID));
        $this->interviewID=htmlspecialchars(strip_tags($this->interviewID));
        $this->interviewType=htmlspecialchars(strip_tags($this->interviewType));
        $this->location=htmlspecialchars(strip_tags($this->location));
        $this->schedule=htmlspecialchars(strip_tags($this->schedule));
        $this->status=htmlspecialchars(strip_tags($this->status));
    
        // bind values
        $stmt->bindParam(":applicantID", $this->applicantID);
        $stmt->bindParam(":interviewID", $this->interviewID);
        $stmt->bindParam(":interviewType", $this->interviewType);
        $stmt->bindParam(":location", $this->location);
        $stmt->bindParam(":schedule", $this->schedule);
        $stmt->bindParam(":status", $this->status);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    function readOne(){
    
        // query to read single record
        $query = "SELECT
                    it.name as interviewTypeName, i.id, i.applicantID, i.interviewID, i.location, i.schedule, i.status
                FROM
                    " . $this->table_name . " i
                    LEFT JOIN
                    interviewtype it
                        ON i.interviewType = it.id
                WHERE
                    i.applicantID = ?
                LIMIT
                    0,2";
    
        // prepare query statement
        $stmt = $this->connect->prepare( $query );
    
        // bind id of product to be updated
        $stmt->bindParam(1, $this->id);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }
}
?>