-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 05, 2019 at 03:13 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ars`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `middlename` text NOT NULL,
  `lastname` text NOT NULL,
  `application_date` date NOT NULL,
  `position_applied` text NOT NULL,
  `status` text NOT NULL,
  `course` text NOT NULL,
  `school` text NOT NULL,
  `birthdate` date NOT NULL,
  `gender` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `firstname`, `middlename`, `lastname`, `application_date`, `position_applied`, `status`, `course`, `school`, `birthdate`, `gender`) VALUES
(1, 'Mark Jason', 'Valdezis', 'Jagonoy', '2019-09-04', 'Technical Specialist', 'For HR Review', 'Bachelor of Science in Computer Science', 'University of Negros Occidental-Recoletos', '2019-09-04', 'Male'),
(2, 'asdasd', 'asdas', 'asdad', '2019-09-05', 'asdasd', 'New', 'asd', 'adsas', '2019-09-05', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `interview`
--

DROP TABLE IF EXISTS `interview`;
CREATE TABLE IF NOT EXISTS `interview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicantID` int(11) NOT NULL,
  `interviewID` int(11) NOT NULL,
  `interviewType` int(11) NOT NULL,
  `location` varchar(256) NOT NULL,
  `schedule` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interview`
--

INSERT INTO `interview` (`id`, `applicantID`, `interviewID`, `interviewType`, `location`, `schedule`) VALUES
(1, 1, 1, 1, 'Conference Room', '2019-09-15 13:30:00'),
(2, 1, 2, 2, 'HR Office', '2019-09-20 13:30:00'),
(3, 2, 1, 1, 'America Conference Room', '2019-09-28 13:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `interviewtype`
--

DROP TABLE IF EXISTS `interviewtype`;
CREATE TABLE IF NOT EXISTS `interviewtype` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interviewtype`
--

INSERT INTO `interviewtype` (`id`, `name`) VALUES
(1, 'Technical Interview'),
(2, 'Final Interview');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `pin` int(11) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `userID`, `pin`, `role`) VALUES
(1, 1335, 1369, 'bod');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
